const burgerOpener = {
	selectors: {
		burgerBtn: 'header__burger',
		closeClass: 'close',
		openClass: 'open',
		menu: 'header__menu',
	},

	init: function () {
		document.addEventListener('click', e => {
			if (e.target.closest(`.${this.selectors.burgerBtn}`)) {
				const burger = e.target.closest(`.${this.selectors.burgerBtn}`);

				if (burger.classList.contains(this.selectors.closeClass)) {
					this.open();
				} else {
					this.close();
				}
			}
		});
	},

	open: function () {
		document.querySelector(`.${this.selectors.burgerBtn}`).classList.add(this.selectors.openClass);
		document.querySelector(`.${this.selectors.burgerBtn}`).classList.remove(this.selectors.closeClass);
		document.querySelector(`.${this.selectors.menu}`).classList.add(this.selectors.openClass);
	},

	close: function () {
		document.querySelector(`.${this.selectors.burgerBtn}`).classList.remove(this.selectors.openClass);
		document.querySelector(`.${this.selectors.burgerBtn}`).classList.add(this.selectors.closeClass);
		document.querySelector(`.${this.selectors.menu}`).classList.remove(this.selectors.openClass);
	},
};

const accordion = {
	selectors: {
		menu: 'faq-card',
		arrowBtn: 'faq-card__btn',
		openClass: 'open',
		closeClass: 'close',
	},

	init: function () {
		const self = this;

		document.addEventListener('click', e => {
			if (e.target.closest(`.${this.selectors.arrowBtn}`)) {
				const arrowBtn = e.target.closest(`.${this.selectors.arrowBtn}`);
				self.open(arrowBtn);
			}
		});
	},

	open: function (btn) {
		const menu = btn.closest(`.${this.selectors.menu}`);
		if (menu.classList.contains(this.selectors.openClass)) {
			menu.classList.remove(this.selectors.openClass);
		} else {
			menu.classList.add(this.selectors.openClass);
		}
	},
}

window.addEventListener('DOMContentLoaded', function () {

	burgerOpener.init();

	accordion.init();

    const swiper = new Swiper('.swiper', {
        slidesPerView: 1,
        spaceBetween: 16,
        loop: true,
        autoplay: {
          delay: 3000,
        },
        breakpoints: {
          769: {
            slidesPerView: 3,
            spaceBetween: 16,
          },
          993: {
            slidesPerView: 3,
            spaceBetween: 30,
          },
        },
        navigation: {
          nextEl: '.swiper-button-next',
          prevEl: '.swiper-button-prev',
        },
    });
});
